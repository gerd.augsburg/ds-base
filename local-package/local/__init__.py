from pathlib import Path
from os.path import join


def data(rel_path=""):
    return join(Path(__file__).parent.parent.parent.absolute(), "data", rel_path)
